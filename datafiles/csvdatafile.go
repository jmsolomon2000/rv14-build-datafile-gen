package datafiles

import (
	"fmt"
	"time"
)

type CSVDataFile struct {
}

func (cdf *CSVDataFile) AddEntry(date time.Time, hours float64) {
	fmt.Printf("%s, %f\n", date, hours)
}
