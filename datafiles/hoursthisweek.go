package datafiles

// year, weekNum := date.ISOWeek()
// fmt.Println(weekNum)

import (
	"fmt"
	"time"
)

type HoursThisWeekDataFile struct {
	currentDate       time.Time
	dateOfFirstEntry  time.Time
	hoursThisWeek     float64
	hoursLastWeek     float64
	preceeding7Days   float64
	preceeding30Days  float64
	totalHoursWorked  float64
	totalWorkEntries  float64
	hoursPerMonthYear map[int]map[time.Month]float64
}

func NewHoursThisWeekDataFile() *HoursThisWeekDataFile {
	dataFile := HoursThisWeekDataFile{currentDate: time.Now(), hoursThisWeek: 0, dateOfFirstEntry: time.Date(2020, 1, 1, 0, 0, 0, 0, time.Local)}
	dataFile.hoursPerMonthYear = make(map[int]map[time.Month]float64)
	return &dataFile
}

func (thisWeek *HoursThisWeekDataFile) AddEntry(date time.Time, hours float64) {
	entryYear, entryWeek := date.ISOWeek()
	currentYear, currentWeek := thisWeek.currentDate.ISOWeek()
	date7DaysAgo := Bod(thisWeek.currentDate.Add(time.Duration(-24*7) * time.Hour))
	date30DaysAgo := Bod(thisWeek.currentDate.Add(time.Duration(-24*30) * time.Hour))

	if thisWeek.hoursPerMonthYear[date.Year()] == nil {
		thisWeek.hoursPerMonthYear[date.Year()] = make(map[time.Month]float64)
	}
	thisWeek.hoursPerMonthYear[date.Year()][date.Month()] += hours

	if hours > 0 && date.Before(thisWeek.dateOfFirstEntry) {
		thisWeek.dateOfFirstEntry = date
	}

	if hours > 0 {
		thisWeek.totalHoursWorked += hours
		thisWeek.totalWorkEntries++
	}

	if entryYear == currentYear && entryWeek == currentWeek {
		thisWeek.hoursThisWeek += hours
	} else if entryYear == currentYear && entryWeek == (currentWeek-1) {
		thisWeek.hoursLastWeek += hours
	}

	if date7DaysAgo.Before(date) || date7DaysAgo.Equal(date) {
		thisWeek.preceeding7Days += hours
	}

	if date30DaysAgo.Before(date) || date30DaysAgo.Equal(date) {
		thisWeek.preceeding30Days += hours
	}

}

func (thisWeek *HoursThisWeekDataFile) WriteOutput(dest string) {
	fmt.Printf("hours worked this week: %.1f\n", thisWeek.hoursThisWeek)
	fmt.Printf("hours worked last week: %.1f\n", thisWeek.hoursLastWeek)
	fmt.Printf("hours worked last 7 days: %.1f\n", thisWeek.preceeding7Days)
	fmt.Printf("hours worked last 30 days: %.1f\n", thisWeek.preceeding30Days)
	fmt.Print("--------------------------------------\n")
	fmt.Printf("Average hours worked per session: %.1f\n", (thisWeek.totalHoursWorked / thisWeek.totalWorkEntries))
	fmt.Printf("Average hours worked per day: %.1f\n", (thisWeek.totalHoursWorked / (time.Since(thisWeek.dateOfFirstEntry).Hours() / 24)))
	fmt.Printf("Average hours worked per week: %.1f\n", (thisWeek.totalHoursWorked / (time.Since(thisWeek.dateOfFirstEntry).Hours() / 168)))
	fmt.Printf("Average hours worked per month: %.1f\n", (thisWeek.totalHoursWorked / (time.Since(thisWeek.dateOfFirstEntry).Hours() / 720)))
	fmt.Printf("Average days a week worked: %.1f\n", (thisWeek.totalWorkEntries / (time.Since(thisWeek.dateOfFirstEntry).Hours() / 168)))
	fmt.Printf("Average days a month worked: %.1f\n", (thisWeek.totalWorkEntries / (time.Since(thisWeek.dateOfFirstEntry).Hours() / 720)))
	fmt.Print("--------------------------------------\n")

	fmt.Println("Breakdown by Month:")

	for yr, mthMap := range thisWeek.hoursPerMonthYear {
		fmt.Printf("------------- %d ------------\n", yr)

		for index := 1; index <= 12; index++ {
			if mthMap[time.Month(index)] != 0 {
				fmt.Printf("%s : %.1f\n", time.Month(index).String(), mthMap[time.Month(index)])
			}
		}
	}
}

func Bod(t time.Time) time.Time {
	year, month, day := t.Date()
	loc, _ := time.LoadLocation("UTC")
	return time.Date(year, month, day, 0, 0, 0, 0, loc)
}
