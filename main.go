package main

import (
	"bufio"
	"flag"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"bitbucket.org/jmsolomon2000/rv14-build-datafile-gen/datafiles"
	"github.com/gohugoio/hugo/parser/metadecoders"

	/*"github.com/spf13/hugo/parser/pageparser"*/

	/*"reflect"*/
	"time"
)

type Datafile interface {
	AddEntry(date time.Time, hours float64)
	WriteOutput(destination string)
}

func main() {
	// /Users/jareds/Documents/workspace/rv14-build/content/
	source := flag.String("source", "", "Location of hugo posts to process")
	out := flag.String("out", "", "Location to output data file")
	// mode := flag.String("mode", "csv", "Output data file type. CSV only for now.")
	flag.Parse()

	if *source == "" || *out == "" {
		flag.PrintDefaults()
	}

	dataFile := datafiles.NewHoursThisWeekDataFile()

	findMd(*source, dataFile)
	dataFile.WriteOutput(*out)
}

func findMd(dir string, dataFile interface{}) {
	fileInfos, _ := ioutil.ReadDir(dir)
	for _, fileInfo := range fileInfos {
		if fileInfo.IsDir() {
			findMd(path.Join(dir, fileInfo.Name()), dataFile)
		} else {
			fileToProcess := path.Join(dir, fileInfo.Name())
			if filepath.Ext(fileToProcess) == ".md" {
				mdFile, _ := os.Open(fileToProcess)
				reader := bufio.NewReader(mdFile)
				mdFileContents, _ := ioutil.ReadAll(reader)
				/*
					page, _ := pageparser.Parse(reader, pageparser.Config{EnableEmoji: false}) //parser.ReadFrom(mdFile)
					t := page.Iterator()
					meta := make(map[string]string)
					decode := metadecoders.
					for {
						item := t.Next()
						if item.IsFrontMatter() {
							fmt.Println(item)
							tokens := strings.Split(item.ValStr(), ":")
							meta[tokens[0]] = tokens[1]
						}
						if item.IsEOF() || item.IsError() {
							break
						}
					}
				*/
				decode := metadecoders.Decoder{}
				meta, _ := decode.UnmarshalToMap(mdFileContents, metadecoders.YAML)
				dateString := meta["date"].(string)
				//dateString := (meta.(map[string]interface{})["date"]).(string)
				
				//Slice datestring [0:10] to remove time stamp if it's present
				date, _ := time.Parse("2006-01-02", dateString[0:10])
				metaHours := meta["hours"]
				//metaHours := meta.(map[string]interface{})["hours"]
				var hrs float64

				switch metaHours.(type) {
				case int:
					hrs = float64(metaHours.(int))
				default:
					hrs = metaHours.(float64)
				}
				dataFile.(Datafile).AddEntry(date, hrs)
			}
		}
	}
}
